﻿<?php  

//LLamamos al archivo de conexion a bbdd
require('includes/conexion.php');

//llamamos a las librerias que hay en /vendor/
require('vendor/autoload.php');

//Llamamos a la conversion de coordenas utm a lat y long
require('includes/phpcoord.php');

//Le decimos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//ESTO ES PARA DESARROLLO
$twig = new Twig_Environment($loader);

//ESTO ES PARA PRODUCCIÓN
//$twig = new Twig_Environment($loader, array('cache' => 'cache'));

//Vamos a pensar, a ver, a que controlador queremos llamar
//Por defecto, llamamos a blogController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='blogController.php';
}

//Llamo al controlador
require('controllers/'.$c);

//Desconectas
$conexion->close();
?>