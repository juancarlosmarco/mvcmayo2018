<?php  
//Fichero controllers/categoriasController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/categoriaModel.php');
require('models/catalogoModel.php');
$catalogo=new Catalogo();



//Recogemos la accion que queremos realizar con issets($_GET['accion'])

if(isset($_GET['accion'])){
	$accion=$_GET['accion'];

}else{

	$accion='listado';
} //Fin del if (issets($_GET['accion']))

switch($accion){
	case 'ver':
	$categoria=$catalogo->dimeCategoria($_GET['id']);
	echo $twig->render('categoria.html.twig', Array('categoria'=>$categoria));

		break;

	case 'insertar':
	echo $twig->render('categoriaFormulario.html.twig', Array('accion'=>'insercion'));

		break;

	case 'insercion':
	$nombre=$_POST['nombre'];
	$descripcion=$_POST['descripcion'];
	$catalogo->nuevoElemento($nombre, $descripcion);
	header('location: index.php?c=categoriasController.php');
	break;


	case 'borrar':

	$id=$_GET['id'];
	$catalogo->borrarElemento($id);
	header('location: index.php?c=categoriasController.php');
	break;

	
	case 'modificar':


	$categoria=$catalogo->dimeCategoria($_GET['id']);
	echo $twig->render('categoriaFormulario.html.twig', Array('categoria'=>$categoria, 'accion'=>'modificacion'));

	break;


	case 'modificacion':
	$nombre=$_POST['nombre'];
	$descripcion=$_POST['descripcion'];
	$id=$_POST['id'];
	$catalogo->guardarElemento($id, $nombre, $descripcion);
	header('location: index.php?c=categoriasController.php');

	break;


	case 'listado':
	default:
	$categorias=$catalogo->dimeCategorias();
	echo $twig->render('categorias.html.twig', Array('categorias'=>$categorias));

	break;

}
// //Como ya he traido  mi modelo de datos, extraigo los datos
// // que luego le pasare a la vista
// if(isset($_GET['id'])){
// 	$categoria=$catalogo->dimeCategoria($_GET['id']);
// 	echo $twig->render('categoria.html.twig', Array('categoria'=>$categoria));
// }else{
// 	$categorias=$catalogo->dimeCategorias();
// 	echo $twig->render('categorias.html.twig', Array('categorias'=>$categorias));
// }


?>