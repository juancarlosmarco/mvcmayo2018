<?php  
//Fichero models/catalogoModel.php

class Catalogo{

	public $elementos; //Sera un VECTOR de categorias

	public function __construct(){
		$this->elementos=[]; //Le digo que va a ser un VECTOR vacio
	}

	public function dimeCategorias(){
		global $conexion;
		// $sql="SELECT * FROM categorias INNER JOIN productos ON categorias.idCat=productos.idCat ORDER BY nombreCat ASC";
		$sql="SELECT * FROM categorias  ORDER BY nombreCat ASC";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Categoria($registro);
		}
		return $this->elementos;
	}

	public function dimeCategoria($id){
		global $conexion;
		// $sql="SELECT * FROM categorias INNER JOIN productos ON categorias.idCat=productos.idCat  WHERE idCat=$id";

		$sql="SELECT * FROM categorias WHERE idCat=$id ORDER BY nombreCat ASC";

		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$elemento=new Categoria($registro);
		return $elemento;
	}
	
	public function nuevoElemento($nombre,$descripcion){

		global $conexion;
		$sql="INSERT INTO categorias(nombreCat, descripcionCat)VALUES('$nombre','$descripcion')";
		$consulta=$conexion->query($sql);

	}

	public function borrarElemento($id){
		global $conexion;
		$sql="DELETE FROM categorias WHERE idCat=$id";
		$consulta=$conexion->query($sql);
	}

	public function guardarElemento($id, $nombre, $descripcion){
		global $conexion;
		$sql="UPDATE categorias SET nombreCat='$nombre', descripcionCat='$descripcion' WHERE idCat=$id";
		$consulta=$conexion->query($sql);

	}

} //Fin de la class Catalogo
?>
