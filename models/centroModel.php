<?php  
//Fichero models/centroModel.php

class Centro{
	
	public $id;
	public $nombre;
	public $direccion;
	public $cp;
	public $localidad;
	public $longitud;
	public $latitud;

	public function __construct($elemento){
		
		$this->id=$elemento->id;
		$this->nombre=$elemento->title;
		$this->direccion=$elemento->address->{'street-address'};
		$this->cp=$elemento->address->{'postal-code'};
		@$this->localidad=$elemento->address->locality;
		@$this->longitud=$elemento->location->longitude;
		@$this->latitud=$elemento->location->latitude;
	}
} //Fin de la class centro

?>