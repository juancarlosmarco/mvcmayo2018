<?php  
//Fichero models/categoriaModel.php

class Categoria{
	public $id;
	public $nombre;
	public $descripcion;
	//public $precio;
	//public $unidades;
	//public $fecha;
	//public $categoria;
	//public $producto;

	public function __construct($registro){
		$this->id=$registro['idCat'];
		$this->nombre=$registro['nombreCat'];
		$this->descripcion=$registro['descripcionCat'];
		//$this->precio=$registro['precioProd'];
		//$this->unidades=$registro['unidadesProd'];
		//$this->fecha=$registro['fechaAlta'];
		//$this->producto=$registro['nombreProd'];
	}
} //Fin de la class categoria
?>