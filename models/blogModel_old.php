<?php  
//Fichero models/blogModel.php

class Blog{

	public $entradas;

	public function __construct(){
		$this->entradas=[];
	}

	public function dimeEntradas(){

		global $conexion; //Hago alusión a la conexión GLOBAL


		$sql="SELECT * FROM blog ORDER BY fecha DESC";
		$consulta=$conexion->query($sql);
		//$consulta=$GLOBALS['conexion']->query($sql);//POR PROBAR
		while($registro=$consulta->fetch_array()){
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas;

	}



	public function dimeEntrada($id){

		global $conexion; //Hago alusión a la conexión GLOBAL
		$sql="SELECT * FROM blog WHERE id=$id";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->entradas[]=new Post($registro);
		}
		return $this->entradas;

	}


} //Fin de la class Blog
?>