<?php  
//Fichero models/centrosModel.php

class Centros{

	public $elementos; //Sera un VECTOR de centros

	public function __construct(){
		$this->elementos=[]; //Le digo que va a ser un VECTOR vacio
	}


	public function dimeElementos(){
		
		$url='https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=json&file=0&filename=212808-0-espacio-deporte&mgmtid=2b279e1d29efb410VgnVCM2000000c205a0aRCRD&preview=full';
		
		//con esto EXTRAIGO TODO EL CONTENIDO DE ESA URL, y lo guardo en una variable
		$datos=file_get_contents($url);
		//Transformo dichos datos JSON, en OBJETOS PHP
		$info=json_decode($datos);

		foreach($info->{'@graph'} as $elemento){
			$this->elementos[]=new Centro($elemento);
		}

		return $this->elementos;
	}
	
	public function dimeElemento($id){

		$url='https://datos.madrid.es/portal/site/egob/menuitem.ac61933d6ee3c31cae77ae7784f1a5a0/?vgnextoid=00149033f2201410VgnVCM100000171f5a0aRCRD&format=json&file=0&filename=212808-0-espacio-deporte&mgmtid=2b279e1d29efb410VgnVCM2000000c205a0aRCRD&preview=full';
		
		$datos=file_get_contents($url);

		$info=json_decode($datos);
		$graph=$info->{'@graph'};

	foreach($graph as $elemento){

	foreach($info->{'@graph'} as $elemento){
			$elemento=new Centro($elemento);
			//echo "$elemento->id ";
			if($elemento->id == $id) {
				return $elemento;
			}
		}

		return null;
	}

// foreach($info->{'@graph'} as $elemento){
			
// 			//echo "$elemento->id ";
// 			if($elemento->id == $id) {
// 				$elemento=new Centro($elemento);
				
// 			}
// 			return $elemento;
// 		}

		
}


	
} //Fin de la class Catalogo
?>
