<?php  
//Fichero models/almacenModel.php

class Almacen{

	public $elementos; //Sera un VECTOR de Productos

	public function __construct(){
		$this->elementos=[]; //Le digo que va a ser un VECTOR vacio
	}

	public function dimeProductos(){
		global $conexion;
		$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat ORDER BY nombreProd ASC";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Producto($registro);
		}
		return $this->elementos;
	}

	public function dimeProducto($id){
		global $conexion;
		$sql="SELECT * FROM productos INNER JOIN categorias ON productos.idCat=categorias.idCat  WHERE idProd=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$elemento=new Producto($registro);
		return $elemento;
	}
	

} //Fin de la class Almacen
?>